.. 
   SPDX-FileCopyrightText: Copyright © duneuro-py contributors, see file LICENSE.md in module root
.. 
   SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-duneuro-py-exception OR LGPL-3.0-or-later
.. duneuropy documentation master file, created by
   sphinx-quickstart on Mon Oct 31 09:49:28 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to duneuropy's documentation!
=====================================

Contents:

.. toctree::
  duneuropy


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

