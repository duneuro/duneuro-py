.. 
   SPDX-FileCopyrightText: Copyright © duneuro-py contributors, see file LICENSE.md in module root
.. 
   SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-duneuro-py-exception OR LGPL-3.0-or-later
duneuropy in 2d
===============

The main driver class ``MEEGDriver``
------------------------------------
.. currentmodule:: duneuropy
.. autoclass:: MEEGDriver2d
  :members:

the duneuropy-module
--------------------
.. automodule:: duneuropy
  :members: Dipole2d, read_field_vectors_2d, read_projections_2d, PointVTKWriter2d, generate_points_on_sphere_2d, read_dipoles_2d
