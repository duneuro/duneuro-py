.. 
   SPDX-FileCopyrightText: Copyright © duneuro-py contributors, see file LICENSE.md in module root
.. 
   SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-duneuro-py-exception OR LGPL-3.0-or-later
duneuropy in 3d
===============

The main driver class ``MEEGDriver``
------------------------------------
.. currentmodule:: duneuropy
.. autoclass:: MEEGDriver3d
  :members:

the duneuropy-module
--------------------
.. automodule:: duneuropy
  :members: Dipole3d, read_field_vectors_3d, read_projections_3d, analytical_solution_3d, PointVTKWriter3d, generate_points_on_sphere_3d, read_dipoles_3d
