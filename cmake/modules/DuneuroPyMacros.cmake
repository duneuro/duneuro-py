# SPDX-FileCopyrightText: Copyright © duneuro-py contributors, see file LICENSE.md in module root
# SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-duneuro-py-exception OR LGPL-3.0-or-later
# File for module specific CMake tests.
find_package(PythonLibs REQUIRED)
include_directories(${PYTHON_INCLUDE_DIR})

find_package(Sphinx)
