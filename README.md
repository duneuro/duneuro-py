<!--
SPDX-FileCopyrightText: Copyright © duneuro-py contributors, see file LICENSE.md in module root
SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-duneuro-py-exception OR LGPL-3.0-or-later
-->

duneuro-py - python binding for duneuro
=========

This module provides python binding for the duneuro module.

License
-------

The duneuro-py library, headers and test programs are free open-source software,
dual-licensed under version 3 or later of the GNU Lesser General Public License
and version 2 of the GNU General Public License with a special run-time exception.

The library makes use of the pybind11 library which is licensed under BSD-style
license.

See the file [LICENSE.md][0] for full copying permissions.

Links
-----
[0]: LICENSE.md
